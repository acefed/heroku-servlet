package io.gitlab.acefed.herokuservlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

public class HerokuServletApplication extends AbstractHandler {
  public void handle(
      String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    response.setContentType("text/plain; charset=utf-8");
    response.setStatus(HttpServletResponse.SC_OK);
    baseRequest.setHandled(true);
    response.getWriter().println("Hello, World!");
  }

  public static void main(String[] args) throws Exception {
    Server server = new Server(Integer.parseInt(System.getenv("PORT")));
    server.setHandler(new HerokuServletApplication());
    server.start();
    server.join();
  }
}
